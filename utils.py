import datetime as dt
import json


# server functions
def uptime(date_server_start):
    return dt.datetime.now() - date_server_start


def info_start_and_version(start_time, version):
    return f"Server was created {start_time}, server version: {version}"


def convert_to_bytes(obj):
    return bytes(json.dumps(obj), encoding="utf-8")


# client functions
def show_dictionary(dictionary):
    output = "\n"
    command = []

    for key, value in dictionary.items():
        command.append(key + ' - ' + value)

    return output.join(command)
