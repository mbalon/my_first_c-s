import socket
import json
import datetime as dt

import utils

HOST = "127.0.0.1"
PORT = 65432
VERSION = "1.0.12"
ACTIVE_USER = {}


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    s.bind((HOST, PORT))
    server_start_date = dt.datetime.now()
    s.listen()
    conn, addr = s.accept()

    with conn:
        print(f"Connected by {addr}")

        while True:
            data = conn.recv(1024)

            if not data:
                break

            command_sent = json.loads(data)

            match command_sent:
                case "uptime":
                    uptime = utils.uptime(server_start_date)
                    conn.send(utils.convert_to_bytes(f"Server is active for {uptime}"))

                case "info":
                    conn.send(utils.convert_to_bytes(utils.info_start_and_version(server_start_date, VERSION)))

                case "stop":
                    conn.send(utils.convert_to_bytes("Connection end"))
                    ACTIVE_USER = {}
                    conn.close()
                    break

                case "register":
                    data = json.loads(conn.recv(1024))
                    with open("users/users.txt", "r") as file:
                        user = json.loads(file.read())
                        user.append(data)
                    with open("users/users.txt", "w") as file:
                        file.write(json.dumps(user))
                    conn.send(utils.convert_to_bytes("User created"))

                case "create_admin":
                    data = json.loads(conn.recv(1024))
                    with open("users/users.txt", "r") as file:
                        user = json.loads(file.read())
                        user.append(data)
                    with open("users/users.txt", "w") as file:
                        file.write(json.dumps(user))
                    conn.send(utils.convert_to_bytes("User admin created"))

                case "login":
                    data = json.loads(conn.recv(1024))
                    login = data["login"]
                    password = data["password"]

                    with open("users/users.txt", "r") as file:
                        users = json.loads(file.read())
                        # TODO: send info to client if login wasn't find in file
                        user = next(item for item in users if item["login"] == login)
                        if password == user["password"]:
                            conn.send(utils.convert_to_bytes(f"Success"))
                            ACTIVE_USER = user
                        else:
                            conn.send(utils.convert_to_bytes(f"Incorrect password"))

                case "show_data":
                    if ACTIVE_USER:
                        conn.send(utils.convert_to_bytes(ACTIVE_USER))
                    else:
                        conn.send(utils.convert_to_bytes("Sign in to use this command"))

                case "delete":
                    if ACTIVE_USER:
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if not (i['login'] == ACTIVE_USER['login'])]
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))

                        ACTIVE_USER = {}
                        conn.send(utils.convert_to_bytes(f"User has been deleted"))

                    else:
                        conn.send(utils.convert_to_bytes(f"Sign in to use this command"))
                        continue

                case "change_login":
                    if ACTIVE_USER:
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if not (i['login'] == ACTIVE_USER['login'])]
                        ACTIVE_USER = {"login": data, "password": ACTIVE_USER["password"], "role": ACTIVE_USER["role"]}
                        user.append(ACTIVE_USER)
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))
                        conn.send(utils.convert_to_bytes("Login has been changed"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active"))

                case "change_password":
                    if ACTIVE_USER:
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if not (i['login'] == ACTIVE_USER['login'])]
                        ACTIVE_USER = {"login": ACTIVE_USER['login'], "password": data, "role": ACTIVE_USER["role"]}
                        user.append(ACTIVE_USER)
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))
                        conn.send(utils.convert_to_bytes("Password has been changed"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active"))

                case "show_data_admin":
                    if ACTIVE_USER and ACTIVE_USER["role"] == "admin":
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if (i['login'] == data)]
                        user = user[0]
                        conn.send(utils.convert_to_bytes(user))
                    else:
                        conn.send(utils.convert_to_bytes("user not active or not active role"))

                case "add_user_admin":
                    if ACTIVE_USER and ACTIVE_USER["role"] == "admin":
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            user = json.loads(file.read())
                            user.append(data)
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))
                        conn.send(utils.convert_to_bytes("User created"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active or not admin role"))

                case "edit_user_data":
                    if ACTIVE_USER and ACTIVE_USER["role"] == "admin":
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if not (i['login'] == data)]
                        data = json.loads(conn.recv(1024))
                        user.append(data)
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))
                        conn.send(utils.convert_to_bytes("User created"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active or not admin role"))

                case "delete_user":
                    if ACTIVE_USER and ACTIVE_USER["role"] == "admin":
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("users/users.txt", "r") as file:
                            users = json.loads(file.read())
                            user = [i for i in users if not (i['login'] == data)]
                        with open("users/users.txt", "w") as file:
                            file.write(json.dumps(user))
                        conn.send(utils.convert_to_bytes("User removed"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active or not admin role"))

                case "send_message":
                    if ACTIVE_USER:
                        conn.send(utils.convert_to_bytes("user is active"))
                        data = json.loads(conn.recv(1024))
                        with open("messages/message.txt", "r") as file:
                            messages = json.loads(file.read())
                        user = [i for i in messages if i['login'] == data['login']]
                        user = user[0]
                        user["content"].append(data["content"])
                        with open("messages/message.txt", "w") as file:
                            file.write(json.dumps(messages))
                        conn.send(utils.convert_to_bytes("Message sent"))
                    else:
                        conn.send(utils.convert_to_bytes("user not active"))
