import socket
import json

import utils

HOST = "127.0.0.1"
PORT = 65432
VERSION = "1.0.11"

commands = {
    "uptime": "show info about server time life",
    "info": "show info about server creation date and server version",
    "help": "show info about available commands",
    "stop": "shut down connection with server",
    "register": "create new user",
    "login": "sign in user to server",
    "delete": "delete active user's account",
    "change_login": "change active user's login",
    "change_password": "change active user's password",
    "send_message": "send message.txt to another user",
}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))

    while True:
        command = input("Enter command: ")

        match command:
            case "help":
                print(utils.show_dictionary(commands))

            case "uptime":
                s.send(utils.convert_to_bytes(command))
                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "info":
                s.send(utils.convert_to_bytes(command))
                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "stop":
                s.send(utils.convert_to_bytes(command))
                data = json.loads(s.recv(1024))
                print(f"{data}")
                break

            case "register":
                s.send(utils.convert_to_bytes(command))
                login = input("Enter login: ")
                password = input("Enter password: ")
                dict_json = {"login": login, "password": password, "role": "user"}
                s.send(utils.convert_to_bytes(dict_json))

                # TODO: check that login is unique

                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "create_admin":
                s.send(utils.convert_to_bytes(command))
                login = input("Enter login: ")
                password = input("Enter password: ")
                dict_json = {"login": login, "password": password, "role": "admin"}
                s.send(utils.convert_to_bytes(dict_json))

                # TODO: check that login is unique

                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "login":
                s.send(utils.convert_to_bytes(command))
                login = input("Enter login: ")
                password = input("Enter password: ")
                dict_json = {"login": login, "password": password}
                s.send(utils.convert_to_bytes(dict_json))
                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "show_data":
                s.send(utils.convert_to_bytes(command))
                data = json.loads(s.recv(1024))
                print(f"{utils.show_dictionary(data)}")

            case "delete":
                s.send(utils.convert_to_bytes(command))
                data = json.loads(s.recv(1024))
                print(f"{data}")

            case "change_login":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    new_login = input("Enter new login: ")
                    s.send((utils.convert_to_bytes(new_login)))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("You need to sign in to use this command")

            case "change_password":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    new_login = input("Enter new password: ")
                    s.send((utils.convert_to_bytes(new_login)))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("You need to sign in to use this command")

            case "show_data_admin":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    user_login = input("Enter user login to check data: ")
                    s.send(utils.convert_to_bytes(user_login))
                    data = json.loads(s.recv(1024))
                    print(f"{utils.show_dictionary(data)}")
                else:
                    print("Login on your admin account to use this command")

            case "add_user_admin":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    user_login = input("Enter user login: ")
                    user_password = input("Enter user password: ")
                    user_role = input("Enter user role: ")
                    user = {"login": user_login, "password": user_password, "role": user_role}
                    s.send(utils.convert_to_bytes(user))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("Login on your admin account to use this command")

            case "edit_user_data":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    user_login = input("Enter user login to change data: ")
                    s.send(utils.convert_to_bytes(user_login))
                    user_login = input("Enter user login: ")
                    user_password = input("Enter user password: ")
                    user_role = input("Enter user role: ")
                    user = {"login": user_login, "password": user_password, "role": user_role}
                    s.send(utils.convert_to_bytes(user))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("Login on your admin account to use this command")

            case "delete_user":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    user_login = input("Enter user login to remove: ")
                    s.send(utils.convert_to_bytes(user_login))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("Login on your admin account to use this command")

            case "send_message":
                s.send(utils.convert_to_bytes(command))
                ans = json.loads(s.recv(1024))
                if ans == "user is active":
                    user_login = input("Send message to: ")
                    content = input("Message: ")
                    message = {"login": user_login, "content": content}
                    s.send(utils.convert_to_bytes(message))
                    data = json.loads(s.recv(1024))
                    print(f"{data}")
                else:
                    print("You need to sign in to use this command")